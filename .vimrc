" general
let mapleader=" "
syntax on
filetype on
filetype plugin on
if exists('+termguicolors')
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif
set t_Co=256
set cursorline
set t_ZH=[3m
set t_ZR=[23m
set hidden
set encoding=utf8
set number
set ruler
set noea
set noautowrite
set nocompatible
set viminfo+=n~/.vim/cache/.viminfo
set viminfo=<10,'10,/500,:1000,n~/.vim/cache/.viminfo
set backspace=indent,eol,start
set pastetoggle=<C-w>p
set noswapfile
set autoread
set cursorcolumn
set splitbelow
set splitright
set showcmd
set virtualedit+=all
set shell=zsh
set fillchars+=vert:\ 

" tags
set tags=tags;/

" clipboard support
if has("clipboard")
  set clipboard=unnamed " copy to the system clipboard

  if has("unnamedplus") " X11 support
    set clipboard=unnamedplus
  endif
endif

" scrolling/viewport
nnoremap <C-u> <C-u>zz
vnoremap <C-u> <C-u>zz
nnoremap <C-d> <C-d>zz
vnoremap <C-d> <C-d>zz
nnoremap zj 10jzz
vnoremap zj 10jzz
nnoremap zk 10kzz
vnoremap zk 10kzz

" autcompletion
set omnifunc=syntaxcomplete#Complete
set completeopt=longest,menuone
:inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
inoremap <expr> <C-n> pumvisible() ? '<C-n>' :
  \ '<C-n><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'
inoremap <expr> <M-,> pumvisible() ? '<C-n>' :
  \ '<C-x><C-o><C-n><C-p><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'
" open omni completion menu closing previous if open and opening new menu without changing the text
inoremap <expr> <C-Space> (pumvisible() ? (col('.') > 1 ? '<Esc>i<Right>' : '<Esc>i') : '') .
            \ '<C-x><C-o><C-r>=pumvisible() ? "\<lt>C-n>\<lt>C-p>\<lt>Down>" : ""<CR>'
" open user completion menu closing previous if open and opening new menu without changing the text
inoremap <expr> <S-Space> (pumvisible() ? (col('.') > 1 ? '<Esc>i<Right>' : '<Esc>i') : '') .
            \ '<C-x><C-u><C-r>=pumvisible() ? "\<lt>C-n>\<lt>C-p>\<lt>Down>" : ""<CR>'

" text wrapping
set linebreak
set wrap
set list
set listchars=eol:¬,tab:→\ ,trail:•,nbsp:⎵
nnoremap <Leader>f :set wrap!<CR>

" search
set incsearch
set smartcase
set ignorecase
set showmatch
nnoremap <Leader>h :set hlsearch!<CR>

" netrw
let g:netrw_liststyle=3
let g:netrw_banner=0
let g:netrw_altv=1
let g:netrw_browse_split = 4
let g:netrw_winsize = 25

" buffer listing
nnoremap <C-w>b :buffers<CR>:buffer 
nnoremap <C-w>B :Buffers<CR>

" buffer deletion
nnoremap <C-w>q :buffers<CR>:BD
nnoremap <C-w>Q :buffers<CR>:bd

" buffer undo
nnoremap <C-w>u :sp #<CR>

" insert time
nnoremap <leader>4 "=strftime("%c")<CR>p"

" practical vim script to apply macros changes over a visual block
xnoremap @ :<C-u>call ExecuteMacroOverVisualRange()<CR>

function! ExecuteMacroOverVisualRange()
  echo "@".getcmdline()
  execute ":'<,'>normal @".nr2char(getchar())
endfunction

" Open quickfix automatically
augroup qf
  autocmd!
  autocmd QuickFixCmdPost [^l]* cwindow
  autocmd QuickFixCmdPost l*    cwindow
  autocmd VimEnter        *     cwindow
augroup END

" Output reads into a scratch buffer
command! -nargs=* -complete=shellcmd R new | setlocal buftype=nofile bufhidden=hide noswapfile | r !<args>

" swap window panes
function! MarkWindowSwap()
  let g:markedWinNum = winnr()
endfunction

function! DoWindowSwap()
  " Mark destination
  let curNum = winnr()
  let curBuf = bufnr( "%" )
  exe g:markedWinNum . "wincmd w"
  " Switch to source and shuffle dest->source
  let markedBuf = bufnr( "%" )
  " Hide and open so that we aren't prompted and keep history
  exe 'hide buf' curBuf
  " Switch to dest and shuffle source->dest
  exe curNum . "wincmd w"
  " Hide and open so that we aren't prompted and keep history
  exe 'hide buf' markedBuf 
endfunction

nmap <silent> <leader>mw :call MarkWindowSwap()<CR>
nmap <silent> <leader>pw :call DoWindowSwap()<CR>

" filemap indents
augroup filemaps
  au FileType ruby setlocal tabstop=2 softtabstop=2 shiftwidth=2 expandtab
  au FileType python setlocal tabstop=4 softtabstop=4 shiftwidth=4 expandtab encoding=utf-8
  au FileType json setlocal tabstop=2 softtabstop=2 shiftwidth=2 expandtab
  au FileType yaml setlocal tabstop=2 softtabstop=2 shiftwidth=2 expandtab
  au FileType logstash setlocal tabstop=2 softtabstop=2 shiftwidth=2 expandtab
  au FileType html setlocal tabstop=2 softtabstop=2 shiftwidth=2 expandtab
augroup END

" redraw vim when focus gained
au FocusGained,BufEnter * :checktime

" auto change dir when entering the buffer
autocmd BufEnter * silent! lcd %:p:h

" show full file path
set statusline+=%F

" open file from current directory of a term
command GetTermCwd execute 'call term_sendkeys(bufnr("%"), " pwd\<CR>")'
let @v=':GetTermCwd:sleep 700mF lvg_"zyik^vg_"xyi:vsp x/z'
let @n=':GetTermCwd:sleep 700mF lvg_"zyik^vg_"xyi:sp x/z'
let @f=':GetTermCwd:sleep 700m?READMEvg_"zyik^vg_"xyi:sp x/z'
let @e=':GetTermCwd:sleep 700mF lvg_"zyik^vg_"xyi:e x/z'
let @t=':GetTermCwd:sleep 700mF lvg_"zyik^vg_"xyi:tabnew x/z'

" managing root dir of projects for easy fzf searching
function! GetProjectRootPath(root_dir)
  let buffer_path = split(expand("%:p:h"), "/")
  let root_index = index(buffer_path, a:root_dir)

  " return this current dir
  if root_index == -1
    return expand("%:p:h")
  endif

  let path_head = "/" . join(buffer_path[:(root_index + 1)], "/")
  return path_head
endfunction

function! SetRoot(root_dir)
  let g:root_path = GetProjectRootPath(a:root_dir)
endfunction

command! -nargs=1 SR call SetRoot(<f-args>)
command! STR execute 'let root_path = expand("%:p:h")'
command! RTF call fzf#vim#files(g:root_path)
command! RTAg call fzf#vim#ag(<q-args>, {'dir': g:root_path})
command! RF call fzf#vim#files(GetProjectRootPath('dev'))
command! RB call fzf#vim#buffers()
command! RAg call fzf#vim#ag(<q-args>, {'dir': GetProjectRootPath('dev')})
command! BAg execute 'FZFLines'
command! -bang -nargs=* RRg
  \ call fzf#vim#grep("rg --column --hidden --line-number --no-heading --color=always --smart-case ".shellescape(<q-args>), 1, fzf#vim#with_preview({'dir': GetProjectRootPath('dev')}), <bang>0)
command! -bang -nargs=* RTRg
  \ call fzf#vim#grep("rg --column --hidden --line-number --no-heading --color=always --smart-case ".shellescape(<q-args>), 1, fzf#vim#with_preview({'dir': g:root_path}), <bang>0)

" delete hidden buffers
if !exists("*DeleteHiddenBuffers") " Clear all hidden buffers when running
	function DeleteHiddenBuffers() " Vim with the 'hidden' option
		let tpbl=[]
		let closed = 0
		call map(range(1, tabpagenr('$')), 'extend(tpbl, tabpagebuflist(v:val))')
		for buf in filter(range(1, bufnr('$')), 'bufexists(v:val) && index(tpbl, v:val)==-1')
			silent execute 'bwipeout' buf
			let closed += 1
		endfor
		echo "Closed ".closed." hidden buffers"
	endfunction
endif
command! BHD call DeleteHiddenBuffers()

" open urls
nnoremap gx yiW:!xdg-open <cWORD><CR> <C-r>" & <CR><CR>"

" switch back to last tab
if !exists('g:lasttab')
  let g:lasttab = 1
endif
nmap gL :exe "tabn ".g:lasttab<CR>
au TabLeave * let g:lasttab = tabpagenr()

" resize panes
nnoremap <silent> <Leader>- :res -10<CR>
nnoremap <silent> <Leader>= :res +10<CR>
nnoremap <silent> _ :vert res -10<CR>
nnoremap <silent> + :vert res +10<CR>

" diff in situ
nnoremap <Leader>dt :diffthis<CR>
nnoremap <Leader>do :diffoff<CR>

" rest client
function! RestConsole()
  let cmd = input('Place cursor on func and use <C-r><C-w>: ')
  execute "set splitright | vnew | set filetype=sh | read !sh # " . cmd
endfunction
nnoremap <C-k> <Cmd>call RestConsole()<CR>

" vim plugins

" vim fugitive
nnoremap <Leader>gs :Git 
nnoremap <Leader>gd :Gvdiffsplit 
nnoremap <Leader>gf :diffget //2<CR>
nnoremap <Leader>gj :diffget //3<CR>
nnoremap <Leader>gl :Git log -- %<CR>

" fzf
set rtp+=~/.fzf

function! s:line_handler(l)
  let keys = split(a:l, ':\t')
  exec 'buf' keys[0]
  exec keys[1]
  normal! ^zz
endfunction

function! s:buffer_lines()
  let res = []
  for b in filter(range(1, bufnr('$')), 'buflisted(v:val)')
    call extend(res, map(getbufline(b,0,"$"), 'b . ":\t" . (v:key + 1) . ":\t" . v:val '))
  endfor
  return res
endfunction

command! FZFLines call fzf#run({
\   'source':  <sid>buffer_lines(),
\   'sink':    function('<sid>line_handler'),
\   'options': '--extended --nth=3..',
\   'down':    '60%'
\})

" emmet
let g:user_emmet_install_global = 0
autocmd FileType html,css,tsx,jsx,ts,js,erb EmmetInstall

" pdf reading
command! -complete=file -nargs=1 Rpdf :r !pdftotext -nopgbrk <q-args> -

" vim gh line
let g:gh_open_command = 'fn() { echo "$@" | xclip -sel clip; }; fn '

" markdown preview
nnoremap <Leader>md :MarkdownPreview<CR>
nnoremap <Leader>mD :MarkdownPreviewStop<CR>
let g:mkdp_auto_close = 0
let g:mkdp_refresh_slow = 1

" rainbow
let g:rainbow_active = 1

" ale
"" yamllint
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
let g:ale_sign_error = '✘'
let g:ale_sign_warning = '⚠'
let g:ale_lint_on_text_changed = 'never'

" tabline plugin
" entire plugin added because of the archived status of the project
"
" File:        tabline.vim
" Maintainer:  Matthew Kitt <http://mkitt.net/>
" Description: Configure tabs within Terminal Vim.
" Last Change: 2012-10-21
" License:     This program is free software. It comes without any warranty,
"              to the extent permitted by applicable law. You can redistribute
"              it and/or modify it under the terms of the Do What The Fuck You
"              Want To Public License, Version 2, as published by Sam Hocevar.
"              See http://sam.zoy.org/wtfpl/COPYING for more details.
" Based On:    http://www.offensivethinking.org/data/dotfiles/vimrc

" Bail quickly if the plugin was loaded, disabled or compatible is set
if (exists("g:loaded_tabline_vim") && g:loaded_tabline_vim) || &cp
  finish
endif
let g:loaded_tabline_vim = 1

function! Tabline()
  let s = ''
  for i in range(tabpagenr('$'))
    let tab = i + 1
    let winnr = tabpagewinnr(tab)
    let buflist = tabpagebuflist(tab)
    let bufnr = buflist[winnr - 1]
    let bufname = bufname(bufnr)
    let bufmodified = getbufvar(bufnr, "&mod")

    let s .= '%' . tab . 'T'
    let s .= (tab == tabpagenr() ? '%#TabLineSel#' : '%#TabLine#')
    let s .= ' ' . tab .' '
    let s .= (bufname != '' ? fnamemodify(bufname, ':t') . ' ' : '[No Name] ')

    if bufmodified
      let s .= '[+] '
    endif
  endfor

  let s .= '%#TabLineFill#'
  if (exists("g:tablineclosebutton"))
    let s .= '%=%999XX'
  endif
  return s
endfunction
set tabline=%!Tabline()

" vim-hexokinase
let g:Hexokinase_highlighters = ['foreground']

" colorscheme
colorscheme iceberg
set background=light
