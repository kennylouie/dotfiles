#!/bin/bash

set -u

# Exponential backoff: retries a command upon failure, scaling up the delay between retries.
# Example: "expbackoff my_command --with --some --args --maybe"
# @param[expression]: expression to evaluate with exponential backoff
expbackoff() {
	local MAX_RETRIES=${EXPBACKOFF_MAX_RETRIES:-10}
	local BASE=${EXPBACKOFF_BASE:-2}
	local MAX=${EXPBACKOFF_MAX:-600}
	local _failures=0

	while ! "$@"; do
		_failures=$(( $_failures + 1 ))

		if (( $_failures > $MAX_RETRIES )); then
			say "$@" >&2
			say " * Failed, max retries exceeded" >&2
			return 1
		else
			local _seconds=$(( $BASE ** ($_failures - 1) ))
			if (( $_seconds > $MAX )); then
				_seconds=$MAX
			fi
			say "$@" >&2
			say " * $_failures failure(s), retrying in $_seconds second(s)" >&2
			sleep $_seconds
		fi
	done
}

say() {
	printf '[installer]: %s\n' "$1"
}

err() {
	say "error: $1" >&2
	exit 1
}

check_cmd() {
	command -v "$1" > /dev/null 2>&1
}

need_cmd() {
	if ! check_cmd "$1"; then
	    err "need '$1' (command not found)"
	fi
}

assert_nz() {
	if [ -z "$1" ]; then err "assert_nz $2"; fi
}

assert_eq() {
	if [ "$1" != "$2" ]; then
	    say "failed assert_eq: $1 != $2" >&2
	    return 1
	fi
}

assert_dir() {
	if [ ! -d "$1" ]; then err "assert_dir $1"; fi
}

assert_no_file() {
	if [ -f "$1" ]; then say "$1 exists" >&2; return 1; fi
}
