#!/bin/bash

set -u

# list of desired vim plugins to install
# ensure the backslash is added after each plugin except last line
# ensure space in front of each plugin
# as this is a space separated list for bash array compatibility
vim_plugins() {
	cat <<EOF
 https://github.com/tpope/vim-surround.git\
 https://github.com/jiangmiao/auto-pairs.git\
 https://github.com/tpope/vim-sleuth.git\
 https://github.com/tpope/vim-fugitive.git\
 https://github.com/airblade/vim-gitgutter.git\
 https://github.com/w0rp/ale.git\
 https://github.com/qpkorr/vim-bufkill.git\
 https://github.com/tpope/vim-obsession.git\
 https://github.com/tpope/vim-unimpaired.git\
 https://github.com/gyim/vim-boxdraw.git\
 https://github.com/junegunn/fzf.vim.git\
 https://github.com/mattn/emmet-vim.git\
 https://github.com/adelarsq/vim-matchit.git\
 https://github.com/frazrepo/vim-rainbow.git\
 https://github.com/ruanyl/vim-gh-line.git\
 https://github.com/iamcco/markdown-preview.nvim.git\
 https://github.com/tpope/vim-eunuch.git\
 https://github.com/sheerun/vim-polyglot.git\
 https://github.com/RRethy/vim-hexokinase.git\
 https://github.com/kennylouie/rose-pine.git
EOF
}

vim_plugins_deps() {
	cat <<EOF
 ripgrep\
 fzf\
 hexokinase
EOF
}

install_ripgrep() {
	sudo apt install ripgrep 1>&2
}

install_hexokinase() {
	cd $HOME/.vim/pack/plugins/start/vim-hexokinase && make hexokinase 1>&2
}

install_fzf() {
	(cd $INSTALL_DIR && git clone --depth 1 https://github.com/junegunn/fzf.git .fzf && .fzf/install) 1>&2
}
