#!/bin/bash

set -u

zsh_plugins() {
	cat <<EOF
 zsh_autosuggestions\
 zsh_completions
EOF
}

install_zsh_autosuggestions() {
	git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
}

install_zsh_completions() {
	git clone https://github.com/zsh-users/zsh-completions ${ZSH_CUSTOM:-${ZSH:-~/.oh-my-zsh}/custom}/plugins/zsh-completions
}
