#!/bin/bash

set -u

source utils.sh
source vim.sh
source ohmyzsh.sh

usage() {
	cat 1>&2 <<EOF
dotfiles installer

Default behaviour is to do nothing. Please see specific flags to install individual components.

Default install directory is \$HOME. The list of vim plugins to install is in installer/vim.sh. This can be modified to suit the user's needs.

The list of oh-my-zsh plugins to install is in installer/ohmyzsh.sh. This can be modified to suit the user's needs.

Flags:
	-A				Install dotfiles
	-d				Directory to install dotfiles
	-p				Install vim plugins
	-v				Change the install path for vim plugins
	-z				Install zsh plugins
	-h				Prints help information

Optional environment variables:
	- EXPBACKOFF_MAX_RETRIES	max number of retries for backoff operations; default 10
	- EXPBACKOFF_BASE		base value for backoff calculations; default 2
	- EXPBACKOFF_MAX		max value cap for a single backoff; default 600s
EOF
}

INSTALL_DIR="$HOME"
VIM_INSTALL_DIR="$HOME/.vim/pack/plugins/start"
OHMYZSH_INSTALL_DIR="$HOME/.oh-my-zsh"

dotfiles() {
	cat <<EOF
 .vimrc\
 .shellrc\
 .zshrc\
 .gitconfig
EOF
}

main() {
	need_cmd getopts

	local _install_vim_plugins=false
	local _install_dotfiles=false
	local _install_zsh_plugins=false
	while getopts phzd:v:A flag
	do
		case "${flag}" in
			A) _install_dotfiles=true;;
			d) INSTALL_DIR=${OPTARG};;
			p) _install_vim_plugins=true;;
			v) VIM_INSTALL_DIR=${OPTARG};;
			z) _install_zsh_plugins=true;;
			h | *) usage; exit 0;;
		esac
	done
	if [ "$OPTIND" -eq 1 ]; then usage; exit 0; fi

	local _failed=false

	# dotfiles
	if [ "$_install_dotfiles" = true ]; then
		say "Installing dotfiles within $INSTALL_DIR..."

		assert_dir "$INSTALL_DIR"

		local _failed_dotfiles=()
		local _dotfiles=($(dotfiles))
		local _status

		_failed_dotfiles+=($(install_dotfiles ${_dotfiles[*]}))
		_status=$?
		if [ ! "$_status" -eq 0 ]; then
			return $_status
		fi

		if [ ! ${#_failed_dotfiles[@]} -eq 0 ]; then
			say "The following dotfile(s) were not installed correctly:"

			for _dotfile in ${_failed_dotfiles[@]}; do
				echo "$_dotfile"
			done

			_failed=true
		fi

		if [[ ! "${_failed_dotfiles[*]}" =~ ".shellrc" ]]; then
			say "To enable .shellrc, please source this in your shell's config, e.g. .bashrc"
		fi
	fi

	# vim plugins
	if [ "$_install_vim_plugins" = true ]; then
		say "Installing vim plugins within $VIM_INSTALL_DIR..."

		need_cmd git
		assert_dir "$VIM_INSTALL_DIR"

		local _vim_plugins=($(vim_plugins))
		local _vim_plugins_deps=($(vim_plugins_deps))
		local _failed_vim_plugins=()
		local _failed_vim_plugins_deps=()
		local _status

		_failed_vim_plugins+=($(install_vim_plugins ${_vim_plugins[*]}))
		_status=$?
		if [ ! "$_status" -eq 0 ]; then
			return $_status
		fi

		if [ ! ${#_failed_vim_plugins[@]} -eq 0 ]; then
			say "The following vim plugin(s) were not installed correctly:"

			for _vim_plugin in ${_failed_vim_plugins[@]}; do
				echo "$_vim_plugin"
			done

			_failed=true
		fi

		_failed_vim_plugins_deps+=($(install_plugins ${_vim_plugins_deps[*]}))
		_status=$?
		if [ ! "$_status" -eq 0 ]; then
			return $_status
		fi

		if [ ! ${#_failed_vim_plugins_deps[@]} -eq 0 ]; then
			say "The following were not installed correctly:"

			for _dep in ${_failed_vim_plugins_deps[@]}; do
				echo "$_dep"
			done

			_failed=true
		fi
	fi

	# oh-my-zsh plugins
	if [ "$_install_zsh_plugins" = true ]; then
		say "Installing oh-my-zsh plugins"

		need_cmd git

		assert_dir "$OHMYZSH_INSTALL_DIR"

		local _zsh_plugins=($(zsh_plugins))
		local _failed_zsh_plugins=()
		local _status

		_failed_zsh_plugins+=($(install_plugins ${_zsh_plugins[*]}))
		_status=$?
		if [ ! "$_status" -eq 0 ]; then
			return $_status
		fi

		if [ ! ${#_failed_zsh_plugins[@]} -eq 0 ]; then
			say "The following oh-my-zsh plugin(s) were not installed correctly:"

			for _zsh_plugin in ${_failed_zsh_plugins[@]}; do
				echo "$_zsh_plugin"
			done

			_failed=true
		fi
	fi

	if [ "$_failed" = false ]; then
		say "success"
	fi
}

install_dotfiles() {
	local _dotfiles=("$@")
	local _status
	local _failed_dotfiles=()

	for _dotfile in ${_dotfiles[@]}; do
		(assert_no_file "$INSTALL_DIR/$_dotfile" && cp ../$_dotfile $INSTALL_DIR/)
		_status=$?
		if [ ! "$_status" -eq 0 ]; then
			_failed_dotfiles+=("$_dotfile")
		fi
	done

	echo ${_failed_dotfiles[*]}
}

install_vim_plugins() {
	local _vim_plugins=("$@")
	local _failed_vim_plugins=()

	for _vim_plugin in ${_vim_plugins[@]}; do
		local _status

		(cd $VIM_INSTALL_DIR && git clone "$_vim_plugin")
		_status=$?
		if [ ! "$_status" -eq 0 ]; then
			_failed_vim_plugins+=("$_vim_plugin")
		fi
	done

	echo ${_failed_vim_plugins[*]}
}

install_plugins() {
	local _plugins=("$@")
	local _failed=()

	for _plugin in ${_plugins[@]}; do
		local _status

		eval "install_$_plugin"
		_status=$?
		if [ ! "$_status" -eq 0 ]; then
			_failed+=("$_plugin")
		fi
	done

	echo ${_failed[*]}
}

main "$@" || exit 1
