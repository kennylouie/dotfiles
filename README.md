# dotfiles
Collection of scripts and configurations for linux/macos setups.

## Background
Installer for the dotfiles and vim plugins / vim plugin deps can be automated with the installer scripts.

```{bash}
cd installer && bash main.sh -h
```

### Dependencies
Requires:
+ git

## Installing Vim from Source
Included is a bash script to install vim from source with my preferred configurations.

```{bash}
./install_vim
```
