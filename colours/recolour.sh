#!/bin/bash

set -u

source ../installer/utils.sh

_colours_file="colours.json"

colours()
{
  cat "$_colours_file"
}

get_colour()
{
  colours | jq -r ".$1"
}

recolour()
{
  _code="$(get_colour $1)"
  _status=$?
  if [ ! "$_status" -eq 0 ]; then
    return $_status
  fi

  sed -i "s/\$$1/$_code/g" $2
}

main()
{
  need_cmd jq
  need_cmd sed

  _file=$1

  recolour foreground $_file
  recolour background $_file
  recolour black1 $_file
  recolour black2 $_file
  recolour red1 $_file
  recolour red2 $_file
  recolour green1 $_file
  recolour green2 $_file
  recolour yellow1 $_file
  recolour yellow2 $_file
  recolour blue1 $_file
  recolour blue2 $_file
  recolour magenta1 $_file
  recolour magenta2 $_file
  recolour cyan1 $_file
  recolour cyan2 $_file
  recolour white1 $_file
  recolour white2 $_file

  return 0
}

main "$@" || exit 1
